﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class Test : Form
    {

        public static string ChuoiKN = "Data Source=NAM;Initial Catalog = QL_CUAHANG; Integrated Security = True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);

        public void HienThiSTT()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel10.Text = data;
            }
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel11.Text = data;
            }
            else guna2HtmlLabel11.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel12.Text = data;
            }
            else guna2HtmlLabel12.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel13.Text = data;
            }
            else guna2HtmlLabel13.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel14.Text = data;
            }
            else guna2HtmlLabel14.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel15.Text = data;
            }
            else guna2HtmlLabel15.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel16.Text = data;
            }
            else guna2HtmlLabel16.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel17.Text = data;
            }
            else guna2HtmlLabel17.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel18.Text = data;
            }
            else guna2HtmlLabel18.Hide();
            if (reader.Read())
            {
                string data = reader["IDNV"].ToString();
                guna2HtmlLabel19.Text = data;
            }
            else guna2HtmlLabel19.Hide();
            cn.Close();
        }
        public void HienThiHoTen()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel29.Text = data;
            }
            else guna2HtmlLabel29.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel28.Text = data;
            }
            else guna2HtmlLabel28.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel27.Text = data;
            }
            else guna2HtmlLabel27.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel26.Text = data;
            }
            else guna2HtmlLabel26.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel25.Text = data;
            }
            else guna2HtmlLabel25.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel24.Text = data;
            }
            else guna2HtmlLabel24.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel23.Text = data;
            }
            else guna2HtmlLabel23.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel22.Text = data;
            }
            else guna2HtmlLabel22.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel21.Text = data;
            }
            else guna2HtmlLabel21.Hide();
            if (reader.Read())
            {
                string data = reader["HOTEN"].ToString();
                guna2HtmlLabel20.Text = data;
            }
            else guna2HtmlLabel20.Hide();
            cn.Close();
        }
        public void HienThiNamSinh()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel39.Text = data;
            }
            else guna2HtmlLabel39.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel38.Text = data;
            }
            else guna2HtmlLabel38.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel37.Text = data;
            }
            else guna2HtmlLabel37.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel36.Text = data;
            }
            else guna2HtmlLabel36.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel35.Text = data;
            }
            else guna2HtmlLabel35.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel34.Text = data;
            }
            else guna2HtmlLabel34.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel33.Text = data;
            }
            else guna2HtmlLabel33.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel32.Text = data;
            }
            else guna2HtmlLabel32.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel31.Text = data;
            }
            else guna2HtmlLabel31.Hide();
            if (reader.Read())
            {
                string data = reader["NAMSINH"].ToString();
                guna2HtmlLabel30.Text = data;
            }
            else guna2HtmlLabel30.Hide();
            cn.Close();
        }
        public void HienThiSDT()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel49.Text = data;
            }
            else guna2HtmlLabel49.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel48.Text = data;
            }
            else guna2HtmlLabel48.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel47.Text = data;
            }
            else guna2HtmlLabel47.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel46.Text = data;
            }
            else guna2HtmlLabel46.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel45.Text = data;
            }
            else guna2HtmlLabel45.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel44.Text = data;
            }
            else guna2HtmlLabel44.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel43.Text = data;
            }
            else guna2HtmlLabel43.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel42.Text = data;
            }
            else guna2HtmlLabel42.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel41.Text = data;
            }
            else guna2HtmlLabel41.Hide();
            if (reader.Read())
            {
                string data = reader["SDT"].ToString();
                guna2HtmlLabel40.Text = data;
            }
            else guna2HtmlLabel40.Hide();
            cn.Close();
        }
        public void HienThiDiaChi()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel59.Text = data;
            }
            else guna2HtmlLabel59.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel58.Text = data;
            }
            else guna2HtmlLabel58.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel57.Text = data;
            }
            else guna2HtmlLabel57.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel56.Text = data;
            }
            else guna2HtmlLabel56.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel55.Text = data;
            }
            else guna2HtmlLabel55.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel54.Text = data;
            }
            else guna2HtmlLabel54.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel53.Text = data;
            }
            else guna2HtmlLabel53.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel52.Text = data;
            }
            else guna2HtmlLabel52.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel51.Text = data;
            }
            else guna2HtmlLabel51.Hide();
            if (reader.Read())
            {
                string data = reader["DIACHI"].ToString();
                guna2HtmlLabel50.Text = data;
            }
            else guna2HtmlLabel50.Hide();
            cn.Close();
        }
        public void HienThiGioiTinh()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel69.Text = data;
            }
            else guna2HtmlLabel69.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel68.Text = data;
            }
            else guna2HtmlLabel68.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel67.Text = data;
            }
            else guna2HtmlLabel67.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel66.Text = data;
            }
            else guna2HtmlLabel66.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel65.Text = data;
            }
            else guna2HtmlLabel65.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel64.Text = data;
            }
            else guna2HtmlLabel64.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel63.Text = data;
            }
            else guna2HtmlLabel63.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel62.Text = data;
            }
            else guna2HtmlLabel62.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel61.Text = data;
            }
            else guna2HtmlLabel61.Hide();
            if (reader.Read())
            {
                string data = reader["GIOITINH"].ToString();
                guna2HtmlLabel60.Text = data;
            }
            else guna2HtmlLabel60.Hide();
            cn.Close();
        }
        public void HienThiTaiKhoan()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel79.Text = data;
            }
            else guna2HtmlLabel79.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel78.Text = data;
            }
            else guna2HtmlLabel78.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel77.Text = data;
            }
            else guna2HtmlLabel77.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel76.Text = data;
            }
            else guna2HtmlLabel76.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel75.Text = data;
            }
            else guna2HtmlLabel75.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel74.Text = data;
            }
            else guna2HtmlLabel74.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel73.Text = data;
            }
            else guna2HtmlLabel73.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel72.Text = data;
            }
            else guna2HtmlLabel72.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel71.Text = data;
            }
            else guna2HtmlLabel71.Hide();
            if (reader.Read())
            {
                string data = reader["TAIKHOAN"].ToString();
                guna2HtmlLabel70.Text = data;
            }
            else guna2HtmlLabel70.Hide();
            cn.Close();
        }
        public void HienThiMatKhau()
        {
            cn.Open();
            string query = "SELECT * FROM THONGTINCANHAN";
            SqlCommand command = new SqlCommand(query, cn);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel89.Text = data;
            }
            else guna2HtmlLabel89.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel88.Text = data;
            }
            else guna2HtmlLabel88.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel87.Text = data;
            }
            else guna2HtmlLabel87.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel86.Text = data;
            }
            else guna2HtmlLabel86.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel85.Text = data;
            }
            else guna2HtmlLabel85.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel84.Text = data;
            }
            else guna2HtmlLabel84.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel83.Text = data;
            }
            else guna2HtmlLabel83.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel82.Text = data;
            }
            else guna2HtmlLabel82.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel81.Text = data;
            }
            else guna2HtmlLabel81.Hide();
            if (reader.Read())
            {
                string data = reader["MATKHAU"].ToString();
                guna2HtmlLabel80.Text = data;
            }
            else guna2HtmlLabel80.Hide();
            cn.Close();
        }
        public Test()
        {
            InitializeComponent();
        }

        private void guna2HtmlLabel5_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel10_Click(object sender, EventArgs e)
        {

        }


        private void Test_Load(object sender, EventArgs e)
        {
            HienThiSTT();
            HienThiHoTen();
            HienThiNamSinh();
            HienThiSDT();
            HienThiDiaChi();
            HienThiGioiTinh();
            HienThiTaiKhoan();
            HienThiMatKhau();
        }

        private void guna2HtmlLabel16_Click(object sender, EventArgs e)
        {

        }
    }
}
