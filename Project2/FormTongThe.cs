﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class FormTongThe : Form
    {
        private string tenDangNhap;

        private void Form2_Load(object sender, EventArgs e)
        {

        }
        public FormTongThe (string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            FormThongTinCaNhan form3 = new FormThongTinCaNhan(this.tenDangNhap);
            this.Hide();
            form3.ShowDialog();

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            FormKho form4 =   new FormKho(this.tenDangNhap);
            this.Hide();
            form4.ShowDialog();
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            FormSanPham form5 = new FormSanPham(this.tenDangNhap);
            this.Hide();
            form5.ShowDialog();
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            FormTaoHoaDon form6 = new FormTaoHoaDon(this.tenDangNhap);
            this.Hide();
            form6.ShowDialog();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            {
                DialogResult result = MessageBox.Show("Chọn 'Có' nếu bạn muốn xem hóa đơn bán, và ngược lại?", "Chọn loại hóa đơn", MessageBoxButtons.YesNoCancel);

                switch (result)
                {
                    case DialogResult.Yes:
                        FormHoaDonBan formHoaDonBan = new FormHoaDonBan(this.tenDangNhap);
                        this.Hide();
                        formHoaDonBan.ShowDialog();
                        break;
                    case DialogResult.No:
                        FormHoaDonNhap formHoaDonNhap = new FormHoaDonNhap(this.tenDangNhap);
                        this.Hide();
                        formHoaDonNhap.ShowDialog();
                        break;
                    case DialogResult.Cancel:
                        break;
                }
            }
        }
        private void guna2HtmlLabel5_Click(object sender, EventArgs e)
        {

        }
    }
}
