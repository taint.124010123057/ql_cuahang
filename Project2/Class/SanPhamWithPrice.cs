﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Class
{
    internal class SanPhamWithPrice
    {
        public string MaSP { get; set; }
        public string TenSP { get; set; }
        public float GiaBan { get; set; }
        public string Display => $"{TenSP} - {GiaBan.ToString()}"; // Tạo thuộc tính hiển thị tên sản phẩm và giá bán

        public int stt {  get; set; }

        public SanPhamWithPrice(string maSP, string tenSP, float giaBan, int stt)
        {
            MaSP = maSP;
            TenSP = tenSP;
            GiaBan = giaBan;
            this.stt = stt;
        }
    }
}
