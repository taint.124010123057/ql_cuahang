﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    internal class HoaDonBan : SanPham
    {
        protected int IDHD;
        protected int IDNV;
        protected List<SanPham> DanhSachSanPham;
        private DateTime NgayBan;
        private string Khach;
        private float TongTien;
        private float GiamGia;

        public HoaDonBan()
        {
            DanhSachSanPham = new List<SanPham>();
            TongTien = 0;
        }
    }
}
