﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    internal class HDB_SP
    {
        public int MaHDB { get; set; }
        public int MaSP { set; get; }
        public float SoLuong { set; get; }
        public int STT { set; get; }

        public HDB_SP() { }
        public HDB_SP(int mahdb, float soluong, int STT)
        {
            MaHDB = mahdb;
            SoLuong = soluong;
            this.STT = STT;
        }
    }
}
