﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2
{
    internal class SanPham2
    {
        public string MASP { get; set; }
        public string TENSP { get; set; }
        public float SoLuong { get; set; }
        public string HANG { get; set; }
        public string LOAI { get; set; }
        public string DVT { get; set; }

        public float Gianban { get; set; }
        public int STT { get; set; }
        public SanPham2() { }
        public SanPham2(string masp, string tensp)
        {
            MASP = masp;
            TENSP = tensp;
        }
        public SanPham2(string masp, string tensp, string hang, string loai, string dvt, float soluong)
        {
            MASP = masp;
            TENSP = tensp;
            HANG = hang;
            LOAI = loai;
            DVT = dvt;
            SoLuong = soluong;

        }
        public SanPham2(string masp, string tensp, string hang, string loai, string dvt, float soluong, float giaban)
        {
            MASP = masp;
            TENSP = tensp;
            HANG = hang;
            LOAI = loai;
            DVT = dvt;
            SoLuong = soluong;
            Gianban = giaban;

        }
    }
}
