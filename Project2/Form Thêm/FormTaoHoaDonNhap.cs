﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class FormTaoHoaDonNhap : Form
    {

        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);
        private string tenDangNhap;
        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }
        public FormTaoHoaDonNhap(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }
        public FormTaoHoaDonNhap()
        {
            InitializeComponent();
        }

        private void Form11_Load(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (guna2TextBox1.Text.Trim() == "" || guna2TextBox2.Text.Trim() == "" || guna2TextBox3.Text.Trim() == "" || guna2TextBox4.Text.Trim() == "" ||
                guna2TextBox5.Text.Trim() == "" || guna2TextBox6.Text.Trim() == "" || guna2TextBox7.Text.Trim() == "")
            {
                guna2TextBox1.Focus();
                MessageBox.Show("vui lòng nhập thông tin còn thiếu");
            }
            else
            {
                string chuoiSQL = "SET DATEFORMAT DMY INSERT INTO THONGTINCANHAN Values" + " ('" + guna2TextBox1.Text + "','" + guna2TextBox2.Text + "','"
              + guna2TextBox3.Text + "','" + guna2TextBox4.Text + "','" + guna2TextBox5.Text + "','" + guna2TextBox6.Text + "','" + guna2TextBox7.Text + "')";
                ThucThi(chuoiSQL);
                guna2TextBox1.Hide();
                guna2TextBox2.Hide();
                guna2TextBox3.Hide();
                guna2TextBox4.Hide();
                guna2TextBox5.Hide();
                guna2TextBox6.Hide();
                guna2TextBox7.Hide();
                guna2TextBox1.Text = "";
                guna2TextBox2.Text = "";
                guna2TextBox3.Text = "";
                guna2TextBox4.Text = "";
                guna2TextBox5.Text = "";
                guna2TextBox6.Text = "";
                guna2TextBox7.Text = "";
                FormHoaDonNhap form6 = new FormHoaDonNhap(this.tenDangNhap);
                this.Hide();
                form6.ShowDialog();
            }
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHoaDonNhap form6 = new FormHoaDonNhap(this.tenDangNhap);
            form6.ShowDialog();
        }
    }
}
