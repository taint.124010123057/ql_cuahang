﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class FormCapAccount : Form
    {

        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);
        private string tenDangNhap;
        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }
        public FormCapAccount(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }
        private int GetIDNV(SqlConnection connection)
        {
            string getMaHDNQuery = "SELECT ISNULL(MAX(IDNV), 0) FROM THONGTINCANHAN";
            using (SqlCommand getMaHDNCommand = new SqlCommand(getMaHDNQuery, connection))
            {
                return Convert.ToInt32(getMaHDNCommand.ExecuteScalar()) + 1;
            }
        }

        public FormCapAccount()
        {
            InitializeComponent();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (guna2TextBox1.Text.Trim() == "" || guna2TextBox2.Text.Trim() == "" || guna2TextBox3.Text.Trim() == "" || guna2TextBox4.Text.Trim() == "" ||
                guna2TextBox5.Text.Trim() == "" || guna2TextBox6.Text.Trim() == "" || guna2TextBox7.Text.Trim() == "")
            {
                guna2TextBox1.Focus();
                MessageBox.Show("vui lòng nhập thông tin còn thiếu");


            }
            else
            {
                
                cn.Open();
                int IDNV = GetIDNV(cn);
                string query = "SELECT * FROM THONGTINCANHAN WHERE TAIKHOAN = @TAIKHOAN";
                using (SqlCommand command = new SqlCommand(query, cn))
                {
                    command.Parameters.AddWithValue("@TAIKHOAN", guna2TextBox1.Text);

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        MessageBox.Show("Tài khoản đã tồn tại");
                        guna2TextBox1.Focus();

                        cn.Close();
                    }
                    else
                    {
                        cn.Close();
                        string chuoiSQL = "SET DATEFORMAT DMY INSERT INTO THONGTINCANHAN Values" + " ('" + IDNV.ToString() + "','" + guna2TextBox1.Text.Trim() + "','" + guna2TextBox2.Text.Trim() + "',N'"
                                           + guna2TextBox3.Text.Trim() + "','" + guna2TextBox4.Text.Trim() + "',N'" + guna2TextBox5.Text.Trim() + "',N'" + guna2TextBox6.Text.Trim() + "','" + guna2TextBox7.Text.Trim() + "')";
                        ThucThi(chuoiSQL);
                        guna2TextBox1.Hide();
                        guna2TextBox2.Hide();
                        guna2TextBox3.Hide();
                        guna2TextBox4.Hide();
                        guna2TextBox5.Hide();
                        guna2TextBox6.Hide();
                        guna2TextBox7.Hide();
                        guna2TextBox1.Text = "";
                        guna2TextBox2.Text = "";
                        guna2TextBox3.Text = "";
                        guna2TextBox4.Text = "";
                        guna2TextBox5.Text = "";
                        guna2TextBox6.Text = "";
                        guna2TextBox7.Text = "";
                        FormThongTinCaNhan form3 = new FormThongTinCaNhan(this.tenDangNhap);
                        this.Hide();
                        form3.ShowDialog();
                    }    

                }


            }

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormThongTinCaNhan form3 = new FormThongTinCaNhan(tenDangNhap);
            form3.ShowDialog();
        }

        private void Form8_Load(object sender, EventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
    
}
