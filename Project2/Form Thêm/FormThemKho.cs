﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Project2
{
    public partial class FormThemKho : Form
    {
        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);
        private string tenDangNhap;

        public FormThemKho(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            // Kiểm tra các trường nhập liệu trống
            if (string.IsNullOrWhiteSpace(guna2TextBox1.Text) || string.IsNullOrWhiteSpace(guna2TextBox2.Text) ||
                string.IsNullOrWhiteSpace(guna2TextBox3.Text) || string.IsNullOrWhiteSpace(guna2TextBox4.Text) ||
                string.IsNullOrWhiteSpace(guna2TextBox5.Text) || string.IsNullOrWhiteSpace(guna2TextBox6.Text))
            {
                MessageBox.Show("Vui lòng nhập thông tin còn thiếu");
                return;
            }

            // Kiểm tra và chuyển đổi các giá trị số
            if (!int.TryParse(guna2TextBox2.Text, out int soluong) || !int.TryParse(guna2TextBox3.Text, out int gianhap) ||
                !int.TryParse(guna2TextBox4.Text, out int giaban) || soluong <= 0 || gianhap <= 0 || giaban <= 0)
            {
                MessageBox.Show("Số lượng, giá nhập, giá bán phải là giá trị nguyên và lớn hơn 0");
                return;
            }

            try
            {
                cn.Open();

                // Kiểm tra xem sản phẩm có tồn tại trong danh mục sản phẩm không
                string checkSanPhamQuery = "SELECT * FROM SANPHAM WHERE IDSP = @IDSP";
                using (SqlCommand checkSanPhamCommand = new SqlCommand(checkSanPhamQuery, cn))
                {
                    checkSanPhamCommand.Parameters.AddWithValue("@IDSP", guna2TextBox1.Text);

                    using (SqlDataReader reader = checkSanPhamCommand.ExecuteReader())
                    {
                        if (!reader.HasRows)
                        {
                            MessageBox.Show("Mã Sản Phẩm không tồn tại trong danh mục sản phẩm");
                            guna2TextBox1.Focus();
                            return;
                        }
                    }
                }

                // Lấy mã hóa đơn nhập mới
                int maHoaDonNhap = GetMaHoaDonNhap(cn);
                decimal tongTien = soluong * gianhap;

                // Thêm thông tin sản phẩm vào bảng Kho
                string insertKhoQuery = @"
                    INSERT INTO KHO(IDSP, SOLUONG, GIANHAP, GIABAN, NSX, NHH) 
                    VALUES (@IDSP, @SoLuong, @GiaNhap, @GiaBan, @NSX, @NHH)
                ";
                using (SqlCommand insertKhoCommand = new SqlCommand(insertKhoQuery, cn))
                {
                    insertKhoCommand.Parameters.AddWithValue("@IDSP", guna2TextBox1.Text.Trim());
                    insertKhoCommand.Parameters.AddWithValue("@SoLuong", soluong);
                    insertKhoCommand.Parameters.AddWithValue("@GiaNhap", gianhap);
                    insertKhoCommand.Parameters.AddWithValue("@GiaBan", giaban);
                    insertKhoCommand.Parameters.AddWithValue("@NSX", guna2TextBox5.Text.Trim());
                    insertKhoCommand.Parameters.AddWithValue("@NHH", guna2TextBox6.Text.Trim());

                    insertKhoCommand.ExecuteNonQuery();
                    MessageBox.Show("Thêm sản phẩm vào kho thành công");
                }

                // Thêm dữ liệu vào bảng HoaDonNhap
                string insertHoaDonNhapQuery = @"
                    INSERT INTO HoaDonNhap(MAHDN, NgayNhap, TongTien) 
                    VALUES (@MaHDN, @NgayNhap, @TongTien)
                ";
                using (SqlCommand insertHoaDonNhapCommand = new SqlCommand(insertHoaDonNhapQuery, cn))
                {
                    insertHoaDonNhapCommand.Parameters.AddWithValue("@MaHDN", maHoaDonNhap);
                    insertHoaDonNhapCommand.Parameters.AddWithValue("@NgayNhap", DateTime.Now);
                    insertHoaDonNhapCommand.Parameters.AddWithValue("@TongTien", tongTien);

                    insertHoaDonNhapCommand.ExecuteNonQuery();
                }

                // Lấy mã hóa đơn nhập vừa thêm vào
                //maHoaDonNhap = GetMaHoaDonNhap(cn);

                // Thêm thông tin sản phẩm vào bảng HDN_SP
                string insertHDN_SPQuery = @"
                    INSERT INTO HDN_SP(MaHDN, IDSP, SoLuong) 
                    VALUES (@MaHDN, @IDSP, @SoLuong)
                ";
                using (SqlCommand insertHDN_SPCommand = new SqlCommand(insertHDN_SPQuery, cn))
                {
                    insertHDN_SPCommand.Parameters.AddWithValue("@MaHDN", maHoaDonNhap);
                    insertHDN_SPCommand.Parameters.AddWithValue("@IDSP", guna2TextBox1.Text.Trim());
                    insertHDN_SPCommand.Parameters.AddWithValue("@SoLuong", soluong);

                    insertHDN_SPCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Lỗi SQL: " + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
            }
            finally
            {
                cn.Close();
            }

            ClearTextBoxes();
            UpdateFormHoaDonNhap();
        }

        private int GetMaHoaDonNhap(SqlConnection connection)
        {
            string getMaHDNQuery = "SELECT ISNULL(MAX(MaHDN), 0) FROM HoaDonNhap";
            using (SqlCommand getMaHDNCommand = new SqlCommand(getMaHDNQuery, connection))
            {
                return Convert.ToInt32(getMaHDNCommand.ExecuteScalar()) + 1;
            }
        }

        private void ClearTextBoxes()
        {
            guna2TextBox1.Clear();
            guna2TextBox2.Clear();
            guna2TextBox3.Clear();
            guna2TextBox4.Clear();
            guna2TextBox5.Clear();
            guna2TextBox6.Clear();
        }

        private void UpdateFormHoaDonNhap()
        {
            FormHoaDonNhap formHoaDonNhap = Application.OpenForms.OfType<FormHoaDonNhap>().FirstOrDefault();
            if (formHoaDonNhap != null)
            {
                formHoaDonNhap.HienThiHoaDon();
            }
        }

        private void ShowFormKho()
        {
            this.Hide();
            FormKho form4 = new FormKho(tenDangNhap);
            form4.ShowDialog();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            ShowFormKho();
        }
        private void FormThemKho_Load(object sender, EventArgs e)
        {

        }
    }
}