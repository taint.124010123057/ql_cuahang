﻿using Project2.Class;
using Project2.Form_Thêm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls.WebParts;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace Project2
{
    public partial class FormTaoHoaDon : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);

        private string tenDangNhap;
        private int MaNV;
        private int currentHoaDonbanId;
        private List<SanPham2> sanPhamList;


        public FormTaoHoaDon(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
            LayMaNVTheoTenDangNhap();
            sanPhamList = new List<SanPham2>();
        }
        private void LayMaNVTheoTenDangNhap()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();
                    string query = "SELECT IDNV FROM THONGTINCANHAN WHERE TAIKHOAN = @Username";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@Username", tenDangNhap);

                    object result = cmd.ExecuteScalar();
                    if (result != null)
                    {
                        MaNV = Convert.ToInt32(result);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi lấy mã nhân viên: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveHoaDonBan()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(guna2TextBoxTENKH.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin hóa đơn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (currentHoaDonbanId == 0)
                {
                    MessageBox.Show("Vui lòng thêm hóa đơn bán trước khi lưu!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();

                    // Tính tổng tiền từ bảng HDB_SP và Kho dựa trên STT
                    string querySum = @"
                SELECT SUM(HDB_SP.SoLuong * KHO.GiaBan) AS TongTien
                FROM HDB_SP
                INNER JOIN KHO ON HDB_SP.STT = KHO.STT
                WHERE HDB_SP.MaHDB = @MaHDB";
                    SqlCommand cmdSum = new SqlCommand(querySum, cn);
                    cmdSum.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
                    object result = cmdSum.ExecuteScalar();
                    float tongTien = result != DBNull.Value ? Convert.ToSingle(result) : 0;

                    string query1 = "UPDATE HoaDonBan SET TongTien = @TongTien WHERE MaHDB = @MaHDB";
                    SqlCommand cmd1 = new SqlCommand(query1, cn);
                    cmd1.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
                    cmd1.Parameters.AddWithValue("@TongTien", tongTien);
                    cmd1.ExecuteNonQuery();

                    // Cập nhật số lượng trong kho
                    string queryUpdateStock = @"
                UPDATE Kho
                SET SoLuong = SoLuong - HDB_SP.SoLuong
                FROM Kho
                INNER JOIN HDB_SP ON Kho.STT = HDB_SP.STT
                WHERE HDB_SP.MaHDB = @MaHDB";
                    SqlCommand cmdUpdateStock = new SqlCommand(queryUpdateStock, cn);
                    cmdUpdateStock.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
                    cmdUpdateStock.ExecuteNonQuery();

                    MessageBox.Show("Lưu hóa đơn bán thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    guna2TextBoxTONGTIEN.Text = tongTien.ToString();
                }

                ResetForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ResetForm()
        {
            guna2TextBoxTENKH.Clear();
            txtSoluong.Clear();
            sanPhamContainer.Controls.Clear();
            sanPhamList.Clear();
            currentHoaDonbanId = 0;
            LoadSanPhamIntoCBB();
            time.Text = DateTime.Now.ToString();
            guna2TextBoxTONGTIEN.Clear();
        }



        private void FormTaoHoaDon_Load(object sender, EventArgs e)
        {
            currentHoaDonbanId = GetNextMaHDB();
            time.Text = DateTime.Now.ToString();
            LoadSanPhamIntoCBB();
            txtSoluong.Hide();
            guna2TextBoxTONGTIEN.Hide();
            guna2Button2.Hide();
            guna2Button3.Hide();
            SanPhamCBB.Hide();


        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            FormTongThe formTongThe = new FormTongThe(tenDangNhap);
            this.Hide();
            formTongThe.ShowDialog();
            this.Close();
        }
        private int GetNextMaHDB()
        {
            try
            {
                int nextMaHDB = 0;
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();
                    string query = "SELECT ISNULL(MAX(MaHDB), 0) + 1 FROM HoaDonBan";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    object result = cmd.ExecuteScalar();
                    if (result != DBNull.Value)
                    {
                        nextMaHDB = Convert.ToInt32(result);
                    }
                }
                return nextMaHDB;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi lấy mã hóa đơn: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }

        //private void LoadSanPhamIntoCBB()
        //{
        //    string query = "SELECT IDSP, TenSP FROM SanPham";
        //    try
        //    {
        //        using (SqlConnection cn = new SqlConnection(ChuoiKN))
        //        {
        //            cn.Open();
        //            SqlCommand sqlCommand = new SqlCommand(query, cn);
        //            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
        //            DataTable data = new DataTable();
        //            adapter.Fill(data);

        //            // Kiểm tra dữ liệu trả về
        //            if (data.Rows.Count == 0)
        //            {
        //                MessageBox.Show("Không có sản phẩm nào trong cơ sở dữ liệu.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //                return;
        //            }

        //            SanPhamCBB.DataSource = data;
        //            SanPhamCBB.DisplayMember = "TENSP" ;
        //            SanPhamCBB.ValueMember = "IDSP";
        //            SanPhamCBB.SelectedIndex = -1;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Có lỗi khi tải danh sách sản phẩm: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        private void LoadSanPhamIntoCBB()
        {
            string query = "SELECT Kho.STT, SanPham.IDSP, SanPham.TenSP, Kho.GiaBan FROM SanPham INNER JOIN Kho ON SanPham.IDSP = Kho.IDSP";
            try
            {
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();
                    SqlCommand sqlCommand = new SqlCommand(query, cn);
                    SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                    DataTable data = new DataTable();
                    adapter.Fill(data);

                    //Kiểm tra dữ liệu trả về
                    if (data.Rows.Count == 0)
                    {
                        MessageBox.Show("Không có sản phẩm nào trong cơ sở dữ liệu.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    List<SanPhamWithPrice> sanPhamList = new List<SanPhamWithPrice>();
                    foreach (DataRow row in data.Rows)
                    {
                        try
                        {
                            int stt = Convert.ToInt32(row["STT"]);
                            string masp = Convert.ToString(row["IDSP"]);
                            string tenSP = row["TenSP"].ToString();
                            float giaBan = Convert.ToSingle(row["GiaBan"]);
                            SanPhamWithPrice sanPham = new SanPhamWithPrice(masp, tenSP, giaBan, stt);
                            sanPhamList.Add(sanPham);
                        }
                        catch (Exception rowEx)
                        {
                            MessageBox.Show("Có lỗi khi xử lý dữ liệu sản phẩm: " + rowEx.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    SanPhamCBB.DataSource = sanPhamList;
                    SanPhamCBB.DisplayMember = "Display";
                    SanPhamCBB.ValueMember = "STT";
                    SanPhamCBB.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi khi tải danh sách sản phẩm: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadSanPhamList()
        {
            string maSP;
            string query1 = "SELECT * FROM HDB_SP WHERE MaHDB = @mahdb";
            try
            {
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();
                    SqlCommand cmd1 = new SqlCommand(query1, cn);
                    cmd1.Parameters.AddWithValue("@mahdb", currentHoaDonbanId);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
                    DataTable data = new DataTable();
                    adapter.Fill(data);
                    List<HDB_SP> hDB_SPs = new List<HDB_SP>();
                    foreach (DataRow row in data.Rows)
                    {
                        int STT = Convert.ToInt32(row["STT"]);
                        int maHDB = Convert.ToInt32(row["MaHDB"]);
                        float soLuong = Convert.ToSingle(row["SoLuong"]);
                        HDB_SP hDB_SP = new HDB_SP(maHDB, soLuong, STT);
                        hDB_SPs.Add(hDB_SP);
                    }

                    List<SanPham2> sanPhamList = new List<SanPham2>();
                    foreach (HDB_SP hDB_SP in hDB_SPs)
                    {

                        string query3 = "SELECT GiaBan FROM Kho WHERE STT = @STT";
                        SqlCommand cmd3 = new SqlCommand(query3, cn);
                        cmd3.Parameters.AddWithValue("@STT", hDB_SP.STT);
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd3);
                        DataTable dt = new DataTable();
                        sqlDataAdapter.Fill(dt);
                        float giaBan = Convert.ToSingle(dt.Rows[0]["GiaBan"]);

                        string query = "SELECT IDSP FROM KHO WHERE STT = @STT";
                        
                            SqlCommand cmd = new SqlCommand(query, cn);
                            cmd.Parameters.AddWithValue("@STT", hDB_SP.STT);
                            maSP = (string)cmd.ExecuteScalar();
                            // maSP là mã sản phẩm tương ứng với STT
                        

                        string query2 = "SELECT * FROM SanPham WHERE IDSP = @masp";
                        SqlCommand cmd2 = new SqlCommand(query2, cn);
                        cmd2.Parameters.AddWithValue("@masp", maSP);
                        sqlDataAdapter = new SqlDataAdapter(cmd2);
                        DataTable data2 = new DataTable();
                        sqlDataAdapter.Fill(data2);
                        string tenSP = data2.Rows[0]["TenSP"].ToString();
                        string hang = data2.Rows[0]["Hang"].ToString();
                        string loai = data2.Rows[0]["PhanLoai"].ToString();
                        string dvt = data2.Rows[0]["DVT"].ToString();
                        sanPhamList.Add(new SanPham2(maSP, tenSP, hang, loai, dvt, hDB_SP.SoLuong, giaBan));
                    }

                    sanPhamContainer.Controls.Clear();
                    foreach (SanPham2 sanPham in sanPhamList)
                    {
                        UCSanPham uCSanPham = new UCSanPham
                        {
                            Size = new Size(1860, 36)
                        };
                        uCSanPham.Guna2HtmlLabel1.Text = sanPham.MASP.ToString();
                        uCSanPham.Guna2HtmlLabel2.Text = sanPham.TENSP;
                        uCSanPham.Guna2HtmlLabel3.Text = sanPham.HANG;
                        uCSanPham.Guna2HtmlLabel4.Text = sanPham.LOAI;
                        uCSanPham.Guna2HtmlLabel5.Text = sanPham.SoLuong.ToString();
                        uCSanPham.Guna2HtmlLabel6.Text = sanPham.DVT;
                        uCSanPham.Guna2HtmlLabel7.Text = sanPham.Gianban.ToString();
                        sanPhamContainer.Controls.Add(uCSanPham);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi tải danh sách sản phẩm hóa đơn: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            AddHoaDonBan();
        }
        private void AddHoaDonBan()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(guna2TextBoxTENKH.Text))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin hóa đơn!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                currentHoaDonbanId = GetNextMaHDB();
                string tenKH = guna2TextBoxTENKH.Text.Trim();
                DateTime ngayBan = DateTime.Now;

                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();

                    string query1 = "INSERT INTO HoaDonBan (MaHDB, IDNV, TenKH, NgayBan) VALUES (@MaHDB, @IDNV, @TenKH, @NgayBan)";
                    SqlCommand cmd1 = new SqlCommand(query1, cn);
                    cmd1.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
                    cmd1.Parameters.AddWithValue("@IDNV", MaNV);
                    cmd1.Parameters.AddWithValue("@TenKH", tenKH);
                    cmd1.Parameters.AddWithValue("@NgayBan", ngayBan);

                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("Thêm hóa đơn bán thành công! Vui lòng thêm danh sách sản phẩm.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            guna2Button1.Hide();
            guna2TextBoxTENKH.Hide();
            txtSoluong.Show();
            guna2TextBoxTONGTIEN.Show();
            guna2Button2.Show();
            guna2Button3.Show();
            SanPhamCBB.Show();
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (SanPhamCBB.SelectedItem == null)
                {
                    MessageBox.Show("Vui lòng chọn sản phẩm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (string.IsNullOrWhiteSpace(txtSoluong.Text))
                {
                    MessageBox.Show("Vui lòng nhập số lượng sản phẩm.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (!int.TryParse(txtSoluong.Text, out int soLuongSanPham))
                {
                    MessageBox.Show("Số lượng sản phẩm không hợp lệ.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (SanPhamCBB.SelectedValue == null || !int.TryParse(SanPhamCBB.SelectedValue.ToString(), out int selectId))
                {
                    MessageBox.Show("Sản phẩm không hợp lệ.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (currentHoaDonbanId == 0)
                {
                    MessageBox.Show("Vui lòng thêm hóa đơn bán trước khi thêm sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();

                    // Kiểm tra số lượng trong kho
                    string queryCheckQuantity = "SELECT SoLuong FROM Kho WHERE STT = @STT";
                    SqlCommand cmdCheckQuantity = new SqlCommand(queryCheckQuantity, cn);
                    cmdCheckQuantity.Parameters.AddWithValue("@STT", selectId);
                    int soLuongTrongKho = (int)cmdCheckQuantity.ExecuteScalar();

                    if (soLuongSanPham > soLuongTrongKho)
                    {
                        MessageBox.Show("Số lượng sản phẩm trong kho không đủ.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    // Thêm sản phẩm vào hóa đơn
                    string query = "INSERT INTO HDB_SP (MaHDB, STT, SoLuong) VALUES (@MaHDB, @STT, @SoLuong)";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
                    cmd.Parameters.AddWithValue("@STT", selectId);
                    cmd.Parameters.AddWithValue("@SoLuong", soLuongSanPham);
                    cmd.ExecuteNonQuery();
                }

                LoadSanPhamList();
                UpdateTongTien();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi thêm sản phẩm: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //private void UpdateTongTien()
        //{
        //    try
        //    {
        //        using (SqlConnection cn = new SqlConnection(ChuoiKN))
        //        {
        //            cn.Open();

        //            // Kiểm tra xem hóa đơn hiện tại có sản phẩm nào không
        //            string queryCheck = "SELECT COUNT(*) FROM HDB_SP WHERE MaHDB = @MaHDB";
        //            SqlCommand cmdCheck = new SqlCommand(queryCheck, cn);
        //            cmdCheck.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
        //            int count = (int)cmdCheck.ExecuteScalar();

        //            if (count == 0)
        //            {
        //                MessageBox.Show("Hóa đơn hiện tại không có sản phẩm nào.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //                return;
        //            }

        //            // Tính tổng tiền từ bảng HDB_SP và cập nhật vào bảng HoaDonBan
        //            string querySum = @"
        //                              SELECT SUM(HDB_SP.SOLUONG * KHO.GIABAN) AS TongDoanhSo
        //                              FROM HDB_SP
        //                              INNER JOIN KHO ON HDB_SP.IDSP = KHO.IDSP
        //                              WHERE HDB_SP.MaHDB = @maHDB
        //                              GROUP BY KHO.STT;";
        //            SqlCommand cmdSum = new SqlCommand(querySum, cn);
        //            cmdSum.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
        //            object result = cmdSum.ExecuteScalar();
        //            float tongTien = result != DBNull.Value ? Convert.ToSingle(result) : 0;

        //            // Cập nhật tổng tiền vào bảng HoaDonBan
        //            string updateQuery = "UPDATE HoaDonBan SET TongTien = @TongTien WHERE MaHDB = @MaHDB";
        //            SqlCommand updateCmd = new SqlCommand(updateQuery, cn);
        //            updateCmd.Parameters.AddWithValue("@TongTien", tongTien);
        //            updateCmd.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
        //            updateCmd.ExecuteNonQuery();

        //            // Hiển thị tổng tiền lên giao diện
        //            guna2TextBoxTONGTIEN.Text = tongTien.ToString("N2"); // Format số tiền với 2 chữ số thập phân
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Có lỗi xảy ra khi tính tổng tiền: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        private void UpdateTongTien()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();

                    // Truy vấn tính tổng tiền từ bảng HDB_SP và Kho dựa trên STT
                    string querySum = @"
         SELECT SUM(HDB_SP.SoLuong * KHO.GiaBan) AS TongTien
         FROM HDB_SP
         INNER JOIN KHO ON HDB_SP.STT = KHO.STT
         WHERE HDB_SP.MaHDB = @MaHDB";
                    SqlCommand cmdSum = new SqlCommand(querySum, cn);
                    cmdSum.Parameters.AddWithValue("@MaHDB", currentHoaDonbanId);
                    object result = cmdSum.ExecuteScalar();
                    float tongTien = result != DBNull.Value ? Convert.ToSingle(result) : 0;

                    // Cập nhật tổng tiền vào TextBox
                    guna2TextBoxTONGTIEN.Text = tongTien.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra khi tính tổng tiền: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sanPhamContainer_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            SaveHoaDonBan();
            FormTaoHoaDon_Load(sender , e);
            guna2Button1.Show();
            guna2TextBoxTENKH.Show();
            
        }
        private void guna2TextBoxTONGTIEN_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2TextBoxTENKH_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
                
