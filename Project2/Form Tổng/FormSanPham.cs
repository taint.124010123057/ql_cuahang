﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project2
{
    public partial class FormSanPham : Form
    {

        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);
        private string tenDangNhap;

        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }

        public void HienThiSanPham()
        {
            try
            {
                cn.Open();
                SqlDataAdapter Da = new SqlDataAdapter("Select * from SANPHAM", cn);
                //DataSet Ds = new DataSet();
                //Da.Fill(Ds);
                ///// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                //dataGridView1.DataSource = Ds.Tables[0];
                DataTable Dt = new DataTable();
                Da.Fill(Dt);
                /// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                guna2DataGridView1.DataSource = Dt;
                //Giải phóng khi không còn sử dụng 
                Dt.Dispose();
                Da.Dispose();
                cn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi kết nối!");
            }
        }
        public FormSanPham()
        {
            InitializeComponent();
        }
        public FormSanPham(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[1].Value.ToString();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            HienThiSanPham();
            guna2ComboBox1.Hide();
            guna2TextBox1.Hide();
            guna2TextBox2.Hide();
            guna2Button7.Hide();
            guna2ComboBox1.Text = "mã sản phẩm";
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            FormTongThe form2 = new FormTongThe(this.tenDangNhap);
            this.Hide();
            form2.ShowDialog();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            FormThemSanPham form9 = new FormThemSanPham(this.tenDangNhap);
            this.Hide();
            form9.ShowDialog();
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show($"Bạn có muốn xóa {guna2TextBox1.Text}?", "Xác nhận", MessageBoxButtons.OKCancel);


            if (result == DialogResult.OK)
            {

                cn.Open();
                string query = "SELECT * FROM KHO WHERE IDSP = @IDSP";
                using (SqlCommand command = new SqlCommand(query, cn))
                {
                    command.Parameters.AddWithValue("@IDSP", guna2TextBox2.Text);

                    SqlDataReader reader = command.ExecuteReader();

                    //kiểm tra kho còn sản phẩm này không
                    if (reader.HasRows)
                    {
                        //thông báo là còn
                        MessageBox.Show("Trong kho vẫn còn mặt hàng này nên không thể xóa");
                        cn.Close();
                        HienThiSanPham();
                    }
                    else
                    {
                        //trong kho không còn
                        cn.Close();
                        string chuoiSQL = "DELETE SANPHAM where TENSP=N'" + guna2TextBox1.Text + "'";
                        ThucThi(chuoiSQL);
                        HienThiSanPham();
                    }
                }
            }
            else
            {
                HienThiSanPham();
            }

        }

        private void guna2DataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;

            if (rowIndex >= 0 && columnIndex >= 0)
            {
                DataGridViewCell editedCell = guna2DataGridView1.Rows[rowIndex].Cells[columnIndex];
                string newValue = editedCell.Value.ToString();

                if (!string.IsNullOrWhiteSpace(newValue))
                {

                    rowIndex = rowIndex + 1;
                    if (columnIndex == 0)
                    {
                        cn.Open();
                        string query = "SELECT * FROM SANPHAM WHERE IDSP = @IDSP";
                        using (SqlCommand command = new SqlCommand(query, cn))
                        {
                            command.Parameters.AddWithValue("@IDSP", newValue);

                            SqlDataReader reader = command.ExecuteReader();

                            if (reader.HasRows)
                            {
                                cn.Close();
                                MessageBox.Show("Mã Sản Phẩm đã tồn tại");
                                guna2DataGridView1.Focus();
                                HienThiSanPham();
                            }
                            else
                            {
                                cn.Close();
                                //sửa mã sản phẩm
                                string chuoiSQL = ("UPDATE SANPHAM\r\n  SET IDSP = '" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n      FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                                ThucThi(chuoiSQL);
                            }
                        }
                    }
                    if (columnIndex == 1)
                    {
                        //sửa tên sản phẩm
                        string chuoiSQL = ("UPDATE SANPHAM\r\n  SET TENSP = N'" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n      FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 2)
                    {
                        //sửa khối lượng
                        string chuoiSQL = ("UPDATE SANPHAM\r\n  SET KHOILUONG = '" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 3)
                    {
                        //sửa thể tích
                        string chuoiSQL = ("UPDATE SANPHAM\r\n  SET THETICH = '" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 4)
                    {
                        //sửa hãng
                        string chuoiSQL = ("UPDATE SANPHAM\r\n  SET HANG = N'" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 5)
                    {
                        //sửa phân loại
                        string chuoiSQL = ("UPDATE SANPHAM\r\n  SET PHANLOAI = N'" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 6)
                    {
                        //sửa DVT
                        string chuoiSQL = ("UPDATE SANPHAM\r\n  SET DVT = N'" + newValue.ToString() + "'WHERE IDSP = ( \r\n SELECT IDSP \r\n   " +
                            "FROM ( \r\n       SELECT IDSP, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM SANPHAM \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }

                }
                else
                {
                    MessageBox.Show("không được để trống");
                    HienThiSanPham();
                }
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            guna2Button2.Hide();
            guna2Button7.Show();
            guna2ComboBox1.Show();
            guna2TextBox1.Show();

            if (guna2ComboBox1.Text == "mã sản phẩm")
            {
                string ChuoiSQL = ("select * from SANPHAM where IDSP like'%" + guna2TextBox1.Text.Trim() + "%'");
                SqlDataAdapter Da = new SqlDataAdapter(ChuoiSQL, cn);
                DataTable dt = new DataTable();
                Da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
            }
            if (guna2ComboBox1.Text == "tên sản phẩm")
            {
                string ChuoiSQL = ("select * from SANPHAM where TENSP like N'%" + guna2TextBox1.Text.Trim() + "%'");
                SqlDataAdapter Da = new SqlDataAdapter(ChuoiSQL, cn);
                DataTable dt = new DataTable();
                Da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
            }
            guna2TextBox1.Text = "";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void guna2DataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[1].Value.ToString();
            guna2TextBox2.Text = guna2DataGridView1.Rows[i].Cells[0].Value.ToString();
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel3_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel8_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel6_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel4_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel9_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel2_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            this.Form5_Load(sender, e);
            guna2Button4.Show();
            guna2Button2.Show();

        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
