﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project2
{
    public partial class FormKho : Form
    {
        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);
        private string tenDangNhap;
        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }

        public void HienThiKho()
        {
            try
            {
                cn.Open();
                SqlDataAdapter Da = new SqlDataAdapter("SELECT STT,KHO.IDSP,TENSP, SOLUONG, GIANHAP, GIABAN,NSX,NHH,DATEDIFF(day, NSX, NHH) AS HSD\r\nFROM SANPHAM, KHO\r\n" +
                    "WHERE SANPHAM.IDSP = KHO.IDSP", cn);
                //DataSet Ds = new DataSet();
                //Da.Fill(Ds);
                ///// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                //dataGridView1.DataSource = Ds.Tables[0];
                DataTable Dt = new DataTable();
                Da.Fill(Dt);
                /// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                guna2DataGridView1.DataSource = Dt;
                //Giải phóng khi không còn sử dụng 
                Dt.Dispose();
                Da.Dispose();
                cn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi kết nối!");
            }
        }

        public void HienThiGanHet(int number)
        {
            try
            {
                cn.Open();
                SqlDataAdapter Da = new SqlDataAdapter("SELECT KHO.IDSP, TENSP, SUM(SOLUONG)  AS TongSoLuong \r\nFROM SANPHAM, KHO\r\n" +
                    "WHERE SANPHAM.IDSP = KHO.IDSP\r\n GROUP BY KHO.IDSP, TENSP\r\n HAVING SUM(SOLUONG) < " + number, cn);
                //DataSet Ds = new DataSet();
                //Da.Fill(Ds);
                ///// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                //dataGridView1.DataSource = Ds.Tables[0];
                DataTable Dt = new DataTable();
                Da.Fill(Dt);
                /// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                guna2DataGridView1.DataSource = Dt;
                //Giải phóng khi không còn sử dụng 
                Dt.Dispose();
                Da.Dispose();
                cn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi kết nối!");
            }
        }
        public FormKho()
        {
            InitializeComponent();
        }
        public FormKho(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }

        private void guna2HtmlLabel1_Click(object sender, EventArgs e)
        {

        }

        private void Form4_Load(object sender, EventArgs e)
        {
            guna2DataGridView1.DataSource = null;
            HienThiKho();
            guna2ComboBox1.Hide();
            guna2TextBox1.Hide();
            guna2ComboBox1.Text = "mã sản phẩm";
            guna2HtmlLabel12.Hide();
            guna2HtmlLabel13.Hide();
            guna2Button6.Hide();
            guna2Button7.Hide();
            guna2TextBox2.Hide();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            FormTongThe form2 = new FormTongThe(this.tenDangNhap);
            this.Hide();
            form2.ShowDialog();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            FormThemKho form10 = new FormThemKho(tenDangNhap);
            this.Hide();
            form10.ShowDialog();
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show($"Bạn có muốn xóa {guna2TextBox2.Text} ở STT {guna2TextBox1.Text}?", "Xác nhận", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                string chuoiSQL = "DELETE KHO where STT='" + guna2TextBox1.Text + "'";
                ThucThi(chuoiSQL);
                HienThiKho();
            }
            else
            {
                HienThiKho();
            }
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            guna2HtmlLabel1.Hide();
            guna2HtmlLabel2.Hide();
            guna2HtmlLabel4.Hide();
            guna2HtmlLabel5.Hide();
            guna2HtmlLabel6.Hide();
            guna2HtmlLabel7.Hide();
            guna2HtmlLabel8.Hide();
            guna2HtmlLabel9.Hide();
            guna2HtmlLabel10.Hide();
            guna2Button7.Show();
            HienThiGanHet(10);
            guna2HtmlLabel12.Show();
            guna2HtmlLabel13.Show();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            guna2Button2.Hide();
            guna2Button3.Hide();
            guna2Button6.Show();
            guna2ComboBox1.Show();
            guna2TextBox1.Show();

            if (guna2ComboBox1.Text == "mã sản phẩm")
            {
                string ChuoiSQL = ("SELECT STT, KHO.IDSP,TENSP, SOLUONG, GIANHAP, GIABAN,NSX,NHH, DATEDIFF(day, NSX, NHH) AS HSD FROM SANPHAM, KHO WHERE SANPHAM.IDSP = KHO.IDSP AND KHO.IDSP like'%" + guna2TextBox1.Text.Trim() + "%'");
                SqlDataAdapter Da = new SqlDataAdapter(ChuoiSQL, cn);
                DataTable dt = new DataTable();
                Da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
            }
            if (guna2ComboBox1.Text == "tên sản phẩm")
            {
                string ChuoiSQL = ("SELECT STT, KHO.IDSP,TENSP, SOLUONG, GIANHAP, GIABAN,NSX,NHH, DATEDIFF(day, NSX, NHH) AS HSD from SANPHAM, KHO where SANPHAM.IDSP = KHO.IDSP AND SANPHAM.TENSP like N'%" + guna2TextBox1.Text.Trim() + "%'");
                SqlDataAdapter Da = new SqlDataAdapter(ChuoiSQL, cn);
                DataTable dt = new DataTable();
                Da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
            }
            guna2TextBox1.Text = "";
        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {

        }

        private void guna2DataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            guna2DataGridView1.Columns[0].ReadOnly = true;
            guna2DataGridView1.Columns[1].ReadOnly = true;
            guna2DataGridView1.Columns[2].ReadOnly = true;
            guna2DataGridView1.Columns[8].ReadOnly = true;

            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[0].Value.ToString();
            guna2TextBox2.Text = guna2DataGridView1.Rows[i].Cells[2].Value.ToString();

        }

        private void guna2HtmlLabel6_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel2_Click(object sender, EventArgs e)
        {

        }


        private void guna2DataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;
            if (rowIndex >= 0 && columnIndex >= 0)
            {
                DataGridViewCell editedCell = guna2DataGridView1.Rows[rowIndex].Cells[columnIndex];
                string newValue = editedCell.Value.ToString().Trim();

                if (!string.IsNullOrWhiteSpace(newValue))
                {

                    rowIndex = rowIndex + 1;


                    if (columnIndex == 3)
                    {
                        //sửa số lượng
                        string chuoiSQL = ("UPDATE KHO\r\n  SET SOLUONG = '" + newValue.ToString() + "'WHERE STT = ( \r\n SELECT STT \r\n   " +
                            "FROM ( \r\n       SELECT STT, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM KHO \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 4)
                    {
                        //sửa giá nhập
                        string chuoiSQL = ("UPDATE KHO\r\n  SET GIANHAP = '" + newValue.ToString() + "'WHERE STT = ( \r\n SELECT STT \r\n   " +
                            "FROM ( \r\n       SELECT STT, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM KHO \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 5)
                    {
                        //sửa giá bán
                        string chuoiSQL = ("UPDATE KHO\r\n  SET GIABAN = '" + newValue.ToString() + "'WHERE STT = ( \r\n SELECT STT \r\n   " +
                            "FROM ( \r\n       SELECT STT, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM KHO \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 6)
                    {
                        //sửa ngày sản xuất
                        string dateStr = newValue; // Sử dụng giá trị mới nhập vào làm chuỗi ngày tháng 
                        DateTime dateValue;
                        if (DateTime.TryParse(dateStr, out dateValue))
                        {
                            if (dateValue <= DateTime.Now)
                            {
                                string formattedDate = dateValue.ToString("dd-MM-yyyy"); // Định dạng ngày tháng 
                                string chuoiSQL = ("SET DATEFORMAT DMY \r\n UPDATE KHO\r\n  SET NSX = '" + formattedDate + "'WHERE STT = ( \r\n SELECT STT \r\n   " +
                                "FROM ( \r\n       SELECT STT, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM KHO \r\n   ) " +
                                "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                                ThucThi(chuoiSQL);
                                HienThiKho();
                            }
                            else
                            {
                                MessageBox.Show("Ngày sản xuất không được lớn hơn hiện tại.");
                                HienThiKho();
                            }

                        }
                    }
                    if (columnIndex == 7)
                    {
                        //sửa ngày hết hạn
                        string dateStr = newValue; // Sử dụng giá trị mới nhập vào làm chuỗi ngày tháng 
                        DateTime dateValue;


                        if (DateTime.TryParse(dateStr, out dateValue))
                        {
                            string formattedDate = dateValue.ToString("dd-MM-yyyy"); // Định dạng ngày tháng 
                            string chuoiSQL = ("SET DATEFORMAT DMY \r\n UPDATE KHO\r\n  SET NHH = '" + formattedDate + "'WHERE STT = ( \r\n SELECT STT \r\n   " +
                            "FROM ( \r\n       SELECT STT, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM KHO \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                            ThucThi(chuoiSQL);
                            HienThiKho();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("không được để trống");
                    HienThiKho();
                }
            }
        }

        private void guna2Button6_Click_1(object sender, EventArgs e)
        {
            this.Form4_Load(sender, e);
            guna2Button3.Show();
            guna2Button2.Show();
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            this.Form4_Load(sender, e);
            guna2HtmlLabel1.Show();
            guna2HtmlLabel2.Show();
            guna2HtmlLabel4.Show();
            guna2HtmlLabel5.Show();
            guna2HtmlLabel6.Show();
            guna2HtmlLabel7.Show();
            guna2HtmlLabel8.Show();
            guna2HtmlLabel9.Show();
            guna2HtmlLabel10.Show();
            
        }

        private void guna2Button2_Click_1(object sender, EventArgs e)
        {
            FormThemKho form = new FormThemKho(this.tenDangNhap);
            this.Hide();
            form.ShowDialog();
        }
    }
}
