﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class FormThongTinCaNhan : Form
    {

        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);

        private string tenDangNhap;

        public FormThongTinCaNhan(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }

        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }

        public void HienThiThongTin()
        {
            try
            {
                cn.Open(); 
                SqlDataAdapter Da = new SqlDataAdapter("Select * from THONGTINCANHAN", cn);
                //DataSet Ds = new DataSet();
                //Da.Fill(Ds);
                ///// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                //dataGridView1.DataSource = Ds.Tables[0];
                DataTable Dt = new DataTable();
                Da.Fill(Dt);
                /// Bind dữ liệu vừa lấy được vào GridView để hiển thị 
                guna2DataGridView1.DataSource = Dt;
                //Giải phóng khi không còn sử dụng 
                Dt.Dispose();
                Da.Dispose();
                cn.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi kết nối!");
            }
        }

        public FormThongTinCaNhan()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            HienThiThongTin();
            guna2TextBox1.Hide();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            FormCapAccount form8 = new FormCapAccount(this.tenDangNhap);
            this.Hide();
            form8.ShowDialog();
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show($"Bạn có muốn xóa tài khoản {guna2TextBox1.Text}?", "Xác nhận", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                string chuoiSQL = "DELETE THONGTINCANHAN where TAIKHOAN ='" + guna2TextBox1.Text + "'";
                ThucThi(chuoiSQL);
                HienThiThongTin();
            }
            else
            {
                HienThiThongTin();
            }
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            FormTongThe form2 = new FormTongThe(this.tenDangNhap);
            this.Hide();
            form2.ShowDialog();
        }

        private void guna2DataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {

        }

        private void guna2DataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            guna2DataGridView1.Columns[0].ReadOnly = true;

            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[1].Value.ToString();
        }

        private void guna2HtmlLabel3_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel8_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel9_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel4_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel7_Click(object sender, EventArgs e)
        {

        }

        private void guna2DataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;

            if (rowIndex >= 0 && columnIndex >= 0)
            {

                DataGridViewCell editedCell = guna2DataGridView1.Rows[rowIndex].Cells[columnIndex];
                string newValue = editedCell.Value.ToString().Trim();

                if (!string.IsNullOrWhiteSpace(newValue))
                {
                    rowIndex = rowIndex + 1;

                    if (columnIndex == 1)
                    {

                        cn.Open();
                        string query = "SELECT * FROM THONGTINCANHAN WHERE TAIKHOAN = @TAIKHOAN";
                        using (SqlCommand command = new SqlCommand(query, cn))
                        {
                            command.Parameters.AddWithValue("@TAIKHOAN", newValue);

                            SqlDataReader reader = command.ExecuteReader();

                            if (reader.HasRows)
                            {
                                //kiểm tra có trùng tài khoản không
                                MessageBox.Show("Tài khoản đã tồn tại");
                                guna2DataGridView1.Focus();
                                cn.Close();
                                HienThiThongTin();
                            }
                            else
                            {
                                //sửa tài khoản
                                cn.Close();
                                string chuoiSQL = ("UPDATE THONGTINCANHAN\r\n  SET TAIKHOAN = '" + newValue.ToString() + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                                "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                                "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                                ThucThi(chuoiSQL);
                            }
                        }

                    }
                    if (columnIndex == 2)
                    {
                        //sửa mật khẩu
                        string chuoiSQL = ("UPDATE THONGTINCANHAN\r\n  SET MATKHAU = '" + newValue.ToString() + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                            "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 3)
                    {
                        //sửa Họ tên
                        string chuoiSQL = ("UPDATE THONGTINCANHAN\r\n  SET HOTEN = N'" + newValue.ToString() + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                            "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 4)
                    {
                        //sửa ngày tháng năm sinh
                        string dateStr = newValue; // Sử dụng giá trị mới nhập vào làm chuỗi ngày tháng 
                        DateTime dateValue;

                        if (DateTime.TryParse(dateStr, out dateValue))
                        {

                            if (dateValue <= DateTime.Now)
                            {
                                string formattedDate = dateValue.ToString("dd-MM-yyyy"); // Định dạng ngày tháng 
                                string chuoiSQL = ("SET DATEFORMAT DMY \r\n UPDATE THONGTINCANHAN\r\n  SET NAMSINH = '" + formattedDate + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                                "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                                "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                                ThucThi(chuoiSQL);
                            }
                            else 
                            { 
                                MessageBox.Show("Ngày tháng năm không được lớn hơn hiện tại.");
                                HienThiThongTin();
                            }
                        }

                    }
                    if (columnIndex == 5)
                    {
                        //sửa địa chỉ
                        string chuoiSQL = ("UPDATE THONGTINCANHAN\r\n  SET DIACHI = N'" + newValue.ToString() + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                            "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 6)
                    {
                        //sửa giới tính
                        string chuoiSQL = ("UPDATE THONGTINCANHAN\r\n  SET GIOITINH = N'" + newValue.ToString() + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                            "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                    if (columnIndex == 7)
                    {
                        //sửa số điện thoại
                        string chuoiSQL = ("UPDATE THONGTINCANHAN\r\n  SET SDT = '" + newValue.ToString() + "'WHERE IDNV = ( \r\n SELECT IDNV \r\n   " +
                            "FROM ( \r\n       SELECT IDNV, ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS row_num \r\n       FROM THONGTINCANHAN \r\n   ) " +
                            "AS sub \r\n   WHERE row_num = " + rowIndex) + "\r\n)";
                        ThucThi(chuoiSQL);
                    }
                }
                else
                {
                    MessageBox.Show("không được để trống");
                    HienThiThongTin();
                }

            }
        }
    }
}
