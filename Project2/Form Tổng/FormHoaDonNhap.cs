﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{

    public partial class FormHoaDonNhap : Form
    {
        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);

        private string tenDangNhap;

        public FormHoaDonNhap (string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
        }

        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }

        public void HienThiHoaDon()
        {
            try
            {
                cn.Open();
                string query = @"
    SELECT 
        HDN.MaHDN, 
        HDN.NgayNhap, 
        SUM(HDN_SP.SoLuong) AS TongSoLuong,
        HDN.TongTien
    FROM 
        HoaDonNhap HDN
    INNER JOIN 
        HDN_SP ON HDN.MaHDN = HDN_SP.MaHDN
    GROUP BY
        HDN.MaHDN, 
        HDN.NgayNhap,
        HDN.TongTien";
                SqlDataAdapter da = new SqlDataAdapter(query, cn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
                dt.Dispose();
                da.Dispose();
                cn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi kết nối! " + ex.Message);
                cn.Close();
            }
        }
        public FormHoaDonNhap()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            HienThiHoaDon();
            guna2TextBox1.Hide();
            guna2ComboBox1.Hide();
            guna2Button7.Hide();
            guna2ComboBox1.Text = "mã hóa đơn";
            guna2DateTimePicker1.Hide();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            FormTongThe form2 = new FormTongThe(this.tenDangNhap);
            this.Hide();
            form2.ShowDialog();
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (guna2DataGridView1.SelectedRows.Count > 0)
                {
                    string maHDN = guna2DataGridView1.SelectedRows[0].Cells["MaHDN"].Value.ToString();

                    DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn xóa hóa đơn này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        using (SqlConnection cn = new SqlConnection(ChuoiKN))
                        {
                            cn.Open();

                            string selectQuery = "SELECT IDSP, SoLuong FROM HDN_SP WHERE MaHDN = @MaHDN";
                            List<Tuple<string, int>> productList = new List<Tuple<string, int>>();

                            using (SqlCommand selectCmd = new SqlCommand(selectQuery, cn))
                            {
                                selectCmd.Parameters.AddWithValue("@MaHDN", maHDN);
                                using (SqlDataReader reader = selectCmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        string maSP = reader.GetString(reader.GetOrdinal("IDSP"));
                                        int soLuong = (int)reader.GetInt32(reader.GetOrdinal("SoLuong"));
                                        productList.Add(new Tuple<string, int>(maSP, soLuong));
                                    }
                                }
                            }

                            string deleteHDN_SPQuery = "DELETE FROM HDN_SP WHERE MaHDN = @MaHDN";
                            using (SqlCommand cmdHDN_SP = new SqlCommand(deleteHDN_SPQuery, cn))
                            {
                                cmdHDN_SP.Parameters.AddWithValue("@MaHDN", maHDN);
                                cmdHDN_SP.ExecuteNonQuery();
                            }

                            string deleteHoaDonNhapQuery = "DELETE FROM HoaDonNhap WHERE MaHDN = @MaHDN";
                            using (SqlCommand cmdHoaDonNhap = new SqlCommand(deleteHoaDonNhapQuery, cn))
                            {
                                cmdHoaDonNhap.Parameters.AddWithValue("@MaHDN", maHDN);
                                cmdHoaDonNhap.ExecuteNonQuery();
                            }

                            foreach (var product in productList)
                            {
                                string updateKhoQuery = "UPDATE Kho SET SoLuong = SoLuong - @SoLuong WHERE IDSP = @IDSP";
                                using (SqlCommand updateCmd = new SqlCommand(updateKhoQuery, cn))
                                {
                                    updateCmd.Parameters.AddWithValue("@IDSP", product.Item1);
                                    updateCmd.Parameters.AddWithValue("@SoLuong", product.Item2);
                                    updateCmd.ExecuteNonQuery();
                                }
                            }
                        }

                        HienThiHoaDon();
                    }
                }
                else
                {
                    MessageBox.Show("Vui lòng chọn một hàng để xóa.");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi xóa dữ liệu: " + ex.Message);
            }
        }
        
    



        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[0].Value.ToString();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            guna2ComboBox1.Show();
            guna2TextBox1.Show();
            guna2Button7.Show();
            if (guna2ComboBox1.Text == "mã hóa đơn")
            {
                string ChuoiSQL = ("SELECT  " +
                                   "HDN.MaHDN,  " +
                                   "HDN.NgayNhap,  " +
                                   "SUM(HDN_SP.SoLuong) AS TongSoLuong, " +
                                   "HDN.TongTien " +
                                   "FROM " +
                                   "HoaDonNhap HDN " +
                                   "INNER JOIN " +
                                   "HDN_SP ON HDN.MaHDN = HDN_SP.MaHDN " +
                                   "WHERE HDN.MaHDN LIKE '%" + guna2TextBox1.Text.Trim() + "%' " +
                                   "GROUP BY " +
                                   "HDN.MaHDN,  " +
                                   "HDN.NgayNhap, " +
                                   "HDN.TongTien; ");
                SqlDataAdapter Da = new SqlDataAdapter(ChuoiSQL, cn);
                DataTable dt = new DataTable();
                Da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
            }
            if (guna2ComboBox1.Text == "tên sản phẩm")
            {
                string ChuoiSQL = ("select * from HoaDonNhap where MaHDB like N'%" + guna2TextBox1.Text.Trim() + "%'");
                SqlDataAdapter Da = new SqlDataAdapter(ChuoiSQL, cn);
                DataTable dt = new DataTable();
                Da.Fill(dt);
                guna2DataGridView1.DataSource = dt;
            }
            guna2TextBox1.Text = "";
        }

        private void guna2DataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[0].Value.ToString();
        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            guna2TextBox1.Text = "";
            Form6_Load(sender, e);
        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = guna2ComboBox1.SelectedItem.ToString().Trim();

            if (selectedValue == "ngày nhập")
            {
                guna2DateTimePicker1.Show();
                guna2DateTimePicker1.Value = DateTime.Today;
            }
        }

        private void guna2DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            string searchQuery = @"
SELECT 
    HDN.MaHDN,
    HDN.NgayNhap,
    SUM(HDN_SP.SoLuong) AS TongSoLuong,
    HDN.TongTien 
FROM 
    HoaDonNhap HDN
INNER JOIN 
        HDN_SP ON HDN.MaHDN = HDN_SP.MaHDN
WHERE 1=1 ";

            List<SqlParameter> parameters = new List<SqlParameter>();
            if (guna2DateTimePicker1.Value != DateTime.MinValue)
            {
                searchQuery += " AND HDN.NgayNhap = @NgayNhap";
                parameters.Add(new SqlParameter("NgayNhap", guna2DateTimePicker1.Value.Date));
            }
            searchQuery += @"
GROUP BY
    HDN.MaHDN,
    HDN.NgayNhap,
    HDN.TongTien
ORDER BY
    HDN.MaHDN";
            try
            {
                using (SqlConnection connection = new SqlConnection(ChuoiKN))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(searchQuery, connection))
                    {
                        command.Parameters.AddRange(parameters.ToArray());
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable table = new DataTable();
                            adapter.Fill(table);
                            guna2DataGridView1.DataSource = table;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi tìm kiếm dữ liệu hóa đơn: " + ex.Message);
            }
        }
    }
}
    

