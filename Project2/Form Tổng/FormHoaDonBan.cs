﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TheArtOfDevHtmlRenderer.Adapters;
using static System.ComponentModel.Design.ObjectSelectorEditor;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Project2
{
    public partial class FormHoaDonBan : Form
    {
        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);

        private SqlConnection connection;
        private SqlDataAdapter adapter;
        private DataTable table;
        private string tenDangNhap;

        public FormHoaDonBan(string tenDangNhap)
        {
            InitializeComponent();
            this.tenDangNhap = tenDangNhap;
            guna2DataGridView1.AllowUserToAddRows = false;
            guna2DataGridView1.AllowUserToDeleteRows = false;
            guna2DataGridView1.ReadOnly = false;
            foreach (DataGridViewColumn column in guna2DataGridView1.Columns)
            {
                column.ReadOnly = false;
            }
        }

        public FormHoaDonBan()
        {
            InitializeComponent();
        }
        public void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }
        private void ShowSanPham(string maHDB)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();
                    string query = @"
                SELECT 
                    SP.TenSP,
                    SP.Hang,
                    SP.PhanLoai,
                    SP.DVT,
                    K.GiaBan,
                    HDB_SP.SoLuong
                FROM 
                    HDB_SP
                INNER JOIN 
                    Kho K ON HDB_SP.STT = K.STT
                INNER JOIN 
                    SanPham SP ON SP.IDSP = K.IDSP
                WHERE
                    HDB_SP.MaHDB = @MaHDB";

                    using (SqlDataAdapter da = new SqlDataAdapter(query, cn))
                    {
                        da.SelectCommand.Parameters.AddWithValue("@MaHDB", maHDB);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        guna2DataGridView1.DataSource = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi kết nối! " + ex.Message);
            }
        }
        public void HienThiHoaDon()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(ChuoiKN))
                {
                    cn.Open();
                    string query = @"
                SELECT 
                    HDB.MaHDB,
                    THONGTINCANHAN.HOTEN,
                    HDB.TenKH,
                    HDB.NgayBan,
                    SUM(HDB_SP.SoLuong * K.GiaBan) AS TongTien
                FROM 
                    HoaDonBan HDB
                INNER JOIN 
                    HDB_SP ON HDB.MaHDB = HDB_SP.MaHDB
                INNER JOIN
                    THONGTINCANHAN ON THONGTINCANHAN.IDNV = HDB.MaNV
                INNER JOIN 
                    Kho K ON HDB_SP.STT = K.STT
                GROUP BY
                    HDB.MaHDB,
                    THONGTINCANHAN.HOTEN,
                    HDB.TenKH,
                    HDB.NgayBan";

                    using (SqlDataAdapter da = new SqlDataAdapter(query, cn))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        guna2DataGridView1.DataSource = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi kết nối! " + ex.Message);
            }
        }
        private void guna2HtmlLabel1_Click(object sender, EventArgs e)
        {

        }

        private void Form7_Load(object sender, EventArgs e)
        {
            guna2TextBox1.Text = "";
            HienThiHoaDon();
            guna2TextBox1.Hide();
            guna2ComboBox1.Hide();
            guna2Button7.Hide();
            guna2ComboBox1.Text = "mã hóa đơn bán";
            guna2DateTimePicker1.Hide();
            guna2Button3.Hide();
            guna2HtmlLabel7.Hide();
            guna2HtmlLabel8.Hide();
            guna2HtmlLabel9.Hide();
            guna2HtmlLabel10.Hide();
            guna2HtmlLabel11.Hide();
            guna2HtmlLabel12.Hide();

        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            FormTongThe form2 = new FormTongThe(this.tenDangNhap);
            this.Hide();
            form2.ShowDialog();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            guna2ComboBox1.Show();
            guna2TextBox1.Show();
            guna2Button7.Show();
            string searchQuery = @"
SELECT 
    HDB.MaHDB,
    DN.HOTEN,
    HDB.TenKH,
    HDB.NgayBan, 
    SUM(HDB_SP.SoLuong * K.GiaBan) AS TongTien
FROM 
    HoaDonBan HDB
INNER JOIN 
    HDB_SP ON HDB.MaHDB = HDB_SP.MaHDB
INNER JOIN 
    Kho K ON HDB_SP.IDSP = K.IDSP
INNER JOIN
    THONGTINCANHAN DN ON HDB.IDNV = DN.IDNV
WHERE 1=1";

            List<SqlParameter> parameters = new List<SqlParameter>();

            if (!string.IsNullOrEmpty(guna2TextBox1.Text) && guna2ComboBox1.Text == "tên khách hàng")
            {
                searchQuery += " AND HDB.TenKH LIKE @TenKH ";
                parameters.Add(new SqlParameter("TenKH", $"{guna2TextBox1.Text.Trim()}%"));
            }
            if (!string.IsNullOrEmpty(guna2TextBox1.Text) && guna2ComboBox1.Text == "mã hóa đơn bán")
            {
                searchQuery += " AND HDB.MaHDB LIKE @MaHDB ";
                parameters.Add(new SqlParameter("MaHDB", $"{guna2TextBox1.Text.Trim()}%"));

            }


            searchQuery += @"
GROUP BY
    HDB.MaHDB,
    DN.HOTEN,
    HDB.TenKH,
    HDB.NgayBan
ORDER BY
    HDB.MaHDB";

            try
            {
                using (SqlConnection connection = new SqlConnection(ChuoiKN))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(searchQuery, connection))
                    {
                        command.Parameters.AddRange(parameters.ToArray());
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable table = new DataTable();
                            adapter.Fill(table);
                            guna2DataGridView1.DataSource = table;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi tìm kiếm dữ liệu hóa đơn: " + ex.Message);
            }
        }


        private void guna2Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (guna2DataGridView1.SelectedRows.Count > 0)
                {
                    string maHDB = guna2DataGridView1.SelectedRows[0].Cells["MaHDB"].Value.ToString();

                    DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn xóa hóa đơn này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        using (SqlConnection cn = new SqlConnection(ChuoiKN))
                        {
                            cn.Open();

                            string deleteHDB_SPQuery = "DELETE FROM HDB_SP WHERE MaHDB = @MaHDB";
                            using (SqlCommand cmdHDB_SP = new SqlCommand(deleteHDB_SPQuery, cn))
                            {
                                cmdHDB_SP.Parameters.AddWithValue("@MaHDB", maHDB);
                                cmdHDB_SP.ExecuteNonQuery();
                            }

                            string deleteHoaDonBanQuery = "DELETE FROM HoaDonBan WHERE MaHDB = @MaHDB";
                            using (SqlCommand cmdHoaDonBan = new SqlCommand(deleteHoaDonBanQuery, cn))
                            {
                                cmdHoaDonBan.Parameters.AddWithValue("@MaHDB", maHDB);
                                cmdHoaDonBan.ExecuteNonQuery();
                            }
                        }

                        HienThiHoaDon();
                    }
                }
                else
                {
                    MessageBox.Show("Vui lòng chọn một hàng để xóa.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi xóa dữ liệu: " + ex.Message);
            }
        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            guna2TextBox1.Text = "";
            guna2DateTimePicker1.Value = DateTime.Now;
            Form7_Load(sender, e);
        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = guna2ComboBox1.SelectedItem.ToString().Trim();

            if (selectedValue == "ngày bán")
            {
                guna2DateTimePicker1.Show();
                guna2DateTimePicker1.Value = DateTime.Today;
            }
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void guna2DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            string searchQuery = @"
SELECT 
    HDB.MaHDB,
    DN.HOTEN,
    HDB.TenKH,
    HDB.NgayBan, 
    SUM(HDB_SP.SoLuong * K.GiaBan) AS TongTien
FROM 
    HoaDonBan HDB
INNER JOIN 
    HDB_SP ON HDB.MaHDB = HDB_SP.MaHDB
INNER JOIN 
    Kho K ON HDB_SP.IDSP = K.IDSP
INNER JOIN
    THONGTINCANHAN DN ON HDB.IDNV = DN.IDNV
WHERE 1=1";

            List<SqlParameter> parameters = new List<SqlParameter>();
            if (guna2DateTimePicker1.Value != DateTime.MinValue)
            {
                searchQuery += " AND HDB.NgayBan = @NgayBan";
                parameters.Add(new SqlParameter("NgayBan", guna2DateTimePicker1.Value.Date));
            }
            searchQuery += @"
GROUP BY
    HDB.MaHDB,
    DN.HOTEN,
    HDB.TenKH,
    HDB.NgayBan
ORDER BY
    HDB.MaHDB";
            try
            {
                using (SqlConnection connection = new SqlConnection(ChuoiKN))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(searchQuery, connection))
                    {
                        command.Parameters.AddRange(parameters.ToArray());
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable table = new DataTable();
                            adapter.Fill(table);
                            guna2DataGridView1.DataSource = table;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi tìm kiếm dữ liệu hóa đơn: " + ex.Message);
            }
        }

        private void guna2DataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guna2DataGridView1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void guna2DataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int i;
            i = guna2DataGridView1.CurrentRow.Index;// hiển thị vị trí đang chọn
            guna2TextBox1.Text = guna2DataGridView1.Rows[i].Cells[0].Value.ToString();
            if (e.RowIndex >= 0)
            {
                guna2DataGridView1.ClearSelection();
                DataGridViewRow selectedRow = guna2DataGridView1.Rows[e.RowIndex];
                string maHDB = selectedRow.Cells["MaHDB"].Value.ToString();
                ShowSanPham(maHDB);
                guna2Button3.Show();
                guna2HtmlLabel2.Hide();
                guna2HtmlLabel3.Hide();
                guna2HtmlLabel4.Hide();
                guna2HtmlLabel5.Hide();
                guna2HtmlLabel6.Hide();
                guna2HtmlLabel7.Show();
                guna2HtmlLabel8.Show();
                guna2HtmlLabel9.Show();
                guna2HtmlLabel10.Show();
                guna2HtmlLabel11.Show();
                guna2HtmlLabel12.Show();

            }
            
        }

        private void guna2ComboBox1_ValueMemberChanged(object sender, EventArgs e)
        {
   
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            guna2DataGridView1.ClearSelection() ;
            guna2HtmlLabel2.Show();
            guna2HtmlLabel3.Show();
            guna2HtmlLabel4.Show();
            guna2HtmlLabel5.Show();
            guna2HtmlLabel6.Show();
            Form7_Load(sender, e);
        }
    }
}
