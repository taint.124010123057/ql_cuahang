﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class FormDangNhap : Form
    {

        public static string ChuoiKN = "Data Source=DESKTOP-TEDA5KG;Initial Catalog=QL_CUAHANG;Integrated Security=True";
        public static SqlConnection cn = new SqlConnection(ChuoiKN);

        public static void ThucThi(string caulenh)
        {
            SqlCommand cm = new SqlCommand(caulenh, cn);
            cn.Open();
            cm.ExecuteNonQuery();
            cn.Close();
        }
        public static string TenDangNhap { get;private set; }

        public FormDangNhap()
        {
            InitializeComponent();
        }

        private void guna2TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel1_Click(object sender, EventArgs e)
        {

        }

        private void guna2HtmlLabel2_Click(object sender, EventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            try
            {
                cn.Open();

                string query = "SELECT * FROM THONGTINCANHAN WHERE TAIKHOAN = @taikhoan AND MATKHAU = @matkhau";
                using (SqlCommand command = new SqlCommand(query, cn))
                {
                    command.Parameters.AddWithValue("@taikhoan", guna2TextBox1.Text);
                    command.Parameters.AddWithValue("@matkhau", guna2TextBox2.Text);

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        TenDangNhap = guna2TextBox1.Text;  // Lưu lại tên tài khoản
                        MessageBox.Show("Đăng nhập thành công!");
                        this.Hide();
                        FormTongThe formTongThe = new FormTongThe(TenDangNhap);
                        formTongThe.ShowDialog();
                        this.Show();
                    }
                    else
                    {
                        MessageBox.Show("Đăng nhập thất bại. Vui lòng kiểm tra lại tên người dùng và mật khẩu.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra: " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cn.Close();
            }
        }

        private void guna2HtmlLabel4_Click(object sender, EventArgs e)
        {

        }
    }
}

